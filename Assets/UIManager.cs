﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI currentNameText;
    string currentNameStr;

    public TMP_InputField inputUserNameField;

    void Start()
    {
        currentNameStr = PlayerPrefs.GetString("UserName", "Unknown");
        currentNameText.text = currentNameStr;
    }

    public void SaveData() {
        currentNameStr = inputUserNameField.text;
        currentNameText.text = currentNameStr;
        PlayerPrefs.SetString("UserName", currentNameStr);
    }

    public void EnsureSave() {
        PlayerPrefs.Save();
    }

    public void NextScene() {
        SceneManager.LoadScene("MainScene");
    }

    public void Crash(int i) {
        i += 100;
        Crash(i);
    }
}
