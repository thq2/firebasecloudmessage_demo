﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainUI : MonoBehaviour
{
    public TextMeshProUGUI currentNameText;
    string currentNameStr;

    void Start() {
        currentNameStr = PlayerPrefs.GetString("UserName", "Unknown");
        currentNameText.text = currentNameStr;
    }

    public void BackScene() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Start");
    }
}
